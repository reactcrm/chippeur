var eventuallySet = require('../func.js').setEventually;

var WarehousePrototype = {
  withId: eventuallySet('id'),
  withExternalId: eventuallySet('externalId'),
  withTitle: eventuallySet('title')
};


module.exports = WarehousePrototype;
