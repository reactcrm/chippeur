var eventuallySet = require('../func.js').setEventually;

//todo use inflection
var AddressPrototype = {
  withCompanyName: eventuallySet('companyName'),
  withFirstName: eventuallySet('firstName'),
  withLastName: eventuallySet('lastName'),
  withAddress1: eventuallySet('address1'),
  withAddress2: eventuallySet('address2'),
  withAddress3: eventuallySet('address3'),
  withCity: eventuallySet('city'),
  withRegion: eventuallySet('region'),
  withZipCode: eventuallySet('zipCode'),
  withCountry: eventuallySet('country'),
  withPhoneNumber: eventuallySet('phoneNumber'),
  withEmail: eventuallySet('email')
};


module.exports = AddressPrototype;