var eventuallySet = require('../func.js').setEventually;

//todo use inflection
var CarrierPrototype = {
  withId: eventuallySet('id'),
  withExternalId: eventuallySet('externalId'),
  withTitle: eventuallySet('title'),
  withServiceLevel: eventuallySet('serviceLevel')
};


module.exports = CarrierPrototype;