var Address = require('./Address.js');
var Package = require('./Package.js');
var Carrier = require('./Carrier.js');
var Warehouse = require('./Warehouse.js');

module.exports = {
  newAddress: function createAddress() {
    return Object.create(Address);
  },
  newPackage: function createPackage() {
    return Object.create(Package);
  },
  newCarrier: function createCarrier() {
    return Object.create(Carrier);
  },
  newWarehouse: function createWarehouse() {
    return Object.create(Warehouse);
  }
};
