var eventuallySet = require('../func.js').setEventually;


var PackagePrototype = {
  withId: eventuallySet('id'),
  withExternalId: eventuallySet('externalId'),
  withTitle: eventuallySet('title'),
  withSKU: eventuallySet('sku'),
  withWeightValue: eventuallySet('weightValue'),
  withWeightUnit: eventuallySet('weightUnit'),
  withQuantity: eventuallySet('quantity'),
  withWidth: eventuallySet('width'),
  withLength: eventuallySet('length'),
  withHeight: eventuallySet('height'),
  withLengthUnit: eventuallySet('lengthUnit')
};


module.exports = PackagePrototype;
