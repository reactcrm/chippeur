module.exports = function mapKeys(source, schema, target) {

  var obj = target || {};

  var schemaKeys = Object.keys(schema);
  schemaKeys.forEach(function (skey) {
    if (source[skey]) {
      if (typeof schema[skey] === 'function') {
        schema[skey](obj, source);
      } else {
        obj[schema[skey]] = source[skey];
      }
    }
  });

  return obj;
};
