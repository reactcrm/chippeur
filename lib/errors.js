function ChippeurError(message, original) {
  Error.call(this, message);
  this.message = message;
  this.original = original;
  this.stack=(new Error()).stack;
}

ChippeurError.prototype = Object.create(Error.prototype);
ChippeurError.prototype.name = 'ChippeurError';
ChippeurError.prototype.constructor = ChippeurError;

module.exports = ChippeurError;
