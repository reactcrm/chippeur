var assert = require('assert');
var assign = require('object-assign');
var Promise = require('bluebird');
var mapKeys = require('../mapKeys');
var request = require('request');
var ChippeurError = require('../errors');
var crypto = require('crypto');
var s3UrlEncode = require('node-s3-url-encode');
var url = require('url');

var prototype = {

  sendRequest: function sendRequest(method, action, parameters) {

    var requestEndpoint;
    var requestString;
    var stringToSign;
    var signature;
    var req = Promise.promisify(request[method]);
    var queryObject = {
      AWSAccessKeyId: this.AWSAccessKeyId,
      SellerId: this.SellerId,
      Action: action,
      SignatureMethod: this.signatureMethod || 'HmacSHA256',
      SignatureVersion: this.signatureVersion || '2',
      Version: this.APIVersion,
      Timestamp: (new Date()).toISOString()
    };

    assign(queryObject, parameters || {});

    //sort keys and url encode (s3 version of course !!! )
    requestString = pairAndOrder(queryObject)
      .map(function (tuple) {
        return tuple.map(s3UrlEncode).join('=');
      }).join('&');

    requestEndpoint = url.parse([this.EndPoint, this.ServiceName, this.APIVersion].join('/'));

    //sign
    stringToSign = method.toUpperCase() + '\n'; //first line
    stringToSign += requestEndpoint.hostname + '\n'; //second line
    stringToSign += requestEndpoint.pathname + '\n';//third line
    stringToSign += requestString;//fourth line

    signature = crypto
      .createHmac(queryObject.SignatureMethod.substr(4), this.SecretKey)
      .update(stringToSign)
      .digest('base64');

    //send
    var requestUri = this.EndPoint
      + '/' + this.ServiceName
      + '/' + this.APIVersion
      + '?' + requestString
      + '&Signature=' + s3UrlEncode(signature);

    return req(requestUri, {
      headers: {
        'User-Agent': 'Chippeur V1.0.0 (Language=Javascript)'
      }
    }).spread(function (response, body) {
      return parseResponse(body);
    }).catch(function (err) {
      console.log(err);
    });


    function pairAndOrder(object) {

      var keys = Object.keys(object);
      var tupleList = keys.map(function (key) {
        return [
          key.toString(),
          object[key].toString()
        ]
      });

      return tupleList.sort(function (tupleA, tupleB) {
        return (tupleA[0] < tupleB[0]) ? -1 : (tupleA[0] === tupleB[0] ? 0 : 1);
      })
    }

    function parseResponse(bodyResponse) {
      //todo some shit with xml ...
      console.log(bodyResponse);
    }


  },

  shipOrder: function shipOrder() {
    return this.sendRequest('post', 'ListInboundShipments', {'ShipmentStatusList.member.1': 'CLOSED'})
      .then(function (result) {

      });
  },

  getOrders: function getOrders(params) {
    return this.sendRequest('post', 'ListInboundShipments', {'ShipmentStatusList.member.1': 'CLOSED'})
      .then(function (result) {

      });
  }

};

module.exports = function factory(options) {

  var services = ['FulfillmentInboundShipment'];

  var instance = Object.create(prototype);

  assert(options.EndPoint, 'you must specify an Endpoint');
  assert(options.ServiceName, 'you must specify a Service among ' + services.join(','));
  assert(options.SellerId, 'you must specify a SellerId');
  assert(options.AWSAccessKeyId, 'you must specify a AWSAccessKeyId');

  assign(instance, options);

  return instance;
};