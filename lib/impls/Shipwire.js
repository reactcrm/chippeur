var assign = require('object-assign');
var Promise = require('bluebird');
var request = require('request');
var mapKeys = require('../mapKeys.js');
var ChippeurError = require('../errors');


var shipToSchema = {
  "email": "email",
  "lastName": function (target, src) {
    target.name = [src['lastName'], src['firstName']].join(' ');
  },
  "firstName": function (target, src) {
    target.name = [src['lastName'], src['firstName']].join(' ');
  },
  "companyName": "company",
  "address1": "address1",
  "address2": "address2",
  "address3": "address3",
  "city": "city",
  "region": "state",
  "zipCode": "postalCode",
  "country": "country",
  "phoneNumber": "phone"
};

var packageSchema = {
  sku: 'sku',
  quantity: 'quantity'
};


var prototype = {

  sendRequest: function sendRequest (method, path, options) {

    var http = Promise.promisify(request);
    var url = assign({}, this.endpoint);
    var opts = {
      auth: {
        username: this.username,
        password: this.password
      },
      json: true,
      method: method,
      url: url
    };

    assign(opts, options);
    url.pathname = path;

    return http(opts)
      .spread(function (resp, body) {

        if (resp.statusCode >= 400) {
          throw new Error(body.message);
        }

        if (body.errors && body.errors.length > 0) {
          throw new ChippeurError('Shipwire Error', body);
        }

        return body;
      });
  },

  shipOrder: function shippmentOrder (pakkage, destination, options) {

    if (!pakkage.map) {
      pakkage = [pakkage];
    }

    var shipTo = mapKeys(destination, shipToSchema);
    var items = pakkage.map(function (p) {
      return mapKeys(pakkage, packageSchema)
    });

    return this.sendRequest('POST', '/api/v3/orders', {body: {shipTo: shipTo, items: items}})
      .then(function (result) {

        var item = result.resource || {};
        var data = {};

        if (item.items && item.items.length) {
          item = item.items[0].resource;
        }

        data._original = item;
        data.id = item.id;
        data.trackingCodes = [];
        return data;
      });

  },

  voidShippingOrder: function voidShippingOrder (orderId, options) {
    return this.sendRequest('POST', '/api/v3/orders/' + orderId + '/cancel')
      .then(function (result) {
        return {
          _original: result
        }
      })
  },

  getTrackingInfo: function getTrackingInfo (orderId) {
    return this.sendRequest('GET', '/api/v3/orders/' + orderId + '/trackings')
      .then(function (result) {

        return {
          _original: result,
          trackingUrls: result.resource.items.map(function (item) {
            return item.resource.url;
          }),
          trackingCodes: result.resource.items.map(function (item) {
            return item.resource.tracking;
          }),
          carrier: result.resource.item.carrier
        }
      })
  },

  getOrders: function (params) {
    return this.sendRequest('GET', '/api/v3/orders').then(function (result) {
      var data = result.resource || {};
      data._original = result;
      return data;
    });
  }

};


module.exports = function factory (options) {


  var instance = Object.create(prototype);


  instance.endpoint = {
    protocol: 'https:',
    hostname: options.testMode === true ? 'api.beta.shipwire.com' : 'api.shipwire.com',
    port: 443
  };

  assign(instance, options);

  return instance;

};
