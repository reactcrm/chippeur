var assign = require('object-assign');
var mapKeys = require('../mapKeys');
var Promise = require('bluebird');
var request = require('request');
var ChippeurError = require('../errors');

var shiptoSchema = {
  'companyName': 'company',
  lastName: function (target, src) {
    target.contact = [src.lastName, src.firstName].join(' ');
  },
  firstName: function (target, src) {
    target.contact = [src.lastName, src.firstName].join(' ');
  },
  address1: 'line1',
  address2: 'line2',
  address3: 'line3',
  city: 'city',
  region: 'state',
  zipCode: 'zip_code',
  phoneNumber: 'phone_no'
};

var packageSchema = {
  id: 'id',
  title: 'name',
  weightValue: 'weight',
  weighUnit: 'weight_units',
  lengthUnit: 'dimension_units',
  width: 'width',
  height: 'height',
  length: 'length'
};

var prototype = {

  sendRequest: function sendRequest (method, path, options) {
    var http = Promise.promisify(request);
    var url = assign({}, this.endpoint);
    var opts = {
      auth: {
        username: this.apiKey,
        password: ''
      },
      json: true,
      method: method,
      url: url
    };

    assign(opts, options);
    url.pathname = path;

    return http(opts)
      .spread(function (resp, body) {
        if (resp.statusCode >= 400) {
          throw new ChippeurError(body.message || 'shipping provider Error', body);
        }

        return body;
      });
  },

  /**
   *
   * @param pakkage
   * @param destination
   * @param options
   *
   * from -> Address
   *
   * @returns {*}
   */
  shipOrder: function shipOrder (pakkage, destination, options) {

    options = options || {};
    var shipTo = mapKeys(destination, shiptoSchema);
    var items = mapKeys(pakkage, packageSchema);
    var body = {
      to: shipTo,
      package: items
    };

    if (options.from) {
      body.from = mapKeys(options.from, shiptoSchema);
    }

    return this.sendRequest('POST', '/v1/shipments', {
      body: body
    })
      .then(function (res) {
        return {
          id: res.id,
          _original: res,
          trackingCodes: res.tracking || []
        };
      });
  },


  voidShippingOrder: function voidShippingOrder (orderId, options) {
    return this.sendRequest('POST', '/v1/shipments/' + orderId + '/void')
      .then(function (result) {
        return {
          _orignal: result
        }
      })
  },

  getOrders: function getOrders (params) {
    return this.sendRequest('GET', '/v1/shipments')
      .then(function (result) {
        return {
          items: result.results || [],
          _original: result
        };
      });
  },

  getTrackingInfo: function getTrackingInfo (orderId) {

    return this.sendRequest('GET', '/v1/shipments/' + orderId)
      .then(function (res) {
        return {
          _original: res,
          trackingCodes: res.tracking,
          trackingUrls: [],
          carrier:res.carrier
        }
      })
  }
};

module.exports = function factory (options) {

  var instance = Object.create(prototype);

  //todo toggle to production endpoint
  instance.endpoint = {
    protocol: 'https:',
    port: 443,
    hostname: 'api.postmaster.io'
  };

  assign(instance, options);

  return instance;
};

