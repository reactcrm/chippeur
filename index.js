var shippers = require('./lib/impls');
var models = require('./lib/models');

var catalog = {
  Shipwire: shippers.Shipwire,
  Postmaster: shippers.Postmaster,
  Endicia: shippers.Endicia
};

module.exports = {
  use: function use (serviceName, options) {
    var factory = catalog[serviceName];
    if (!factory) {
      throw new Error('could not find the shipper implementaiton requested: ' + serviceName);
    }
    return factory(options);
  },
  registerShipper: function registerShipper (name, factory) {
    catalog[name] = factory;
  },
  models: models
};


