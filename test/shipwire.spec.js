var credentials = require('./credentials').shipwire;
var shipwire = require('../lib/impls/Shipwire.js');
var assert = require('assert');
var models = require('../lib/models');
var casual = require('casual');
var assign = require('object-assign');

describe('shipwire implementation', function () {


  var service;

  beforeEach(function () {
    service = shipwire(assign(credentials, {testMode: true}));
  });

  describe('get shipping orders', function () {


    it('should get ', function (done) {

      service.getOrders()
        .then(function (result) {
          assert(result._original, 'original should be defined');
          done();
        })
        .catch(function (err) {
          console.log(err);
        });

    });

  });

  describe('send shipping order', function () {

    it('should send a shipping order', function (done) {

      var shipTo = models.newAddress()
        .withCountry(casual.country)
        .withAddress1(casual.address1)
        .withAddress2(casual.address2)
        .withCity(casual.city)
        .withRegion(casual.state_abbr)
        .withFirstName(casual.first_name)
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          assert(res._original);
          assert(res.id);
          assert(res.trackingCodes);
          done();
        })
        .catch(function (err) {
          console.log(err);
        });
    });

    it('should reject a promise when the remote service return an error', function (done) {

      var shipTo = models.newAddress()
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('EVIL')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          throw new Error('should not get here');
        })
        .catch(function (err) {
          assert(err.original);
          done();
        });
    });
  });

  describe('void shipping order', function () {

    it('should void a shipping order', function (done) {

      var shipTo = models.newAddress()
        .withCountry(casual.country)
        .withAddress1(casual.address1)
        .withAddress2(casual.address2)
        .withCity(casual.city)
        .withRegion(casual.state_abbr)
        .withFirstName(casual.first_name)
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          assert(res._original);
          assert(res.id);
          return service.voidShippingOrder(res.id);
        })
        .then(function (result) {
          done();
        })
        .catch(function (err) {
          console.log(err);
        });
    });

    it('should reject a promise when the remote service return an error', function (done) {

      service.voidShippingOrder('whatthafuck')
        .then(function (res) {
          throw new Error('should not get here');
        })
        .catch(function (err) {
          assert.equal(err.message, 'Order not found');
          done();
        });
    });
  });

  describe('get tracking info', function () {


    // can not test if no real data
    xit('should get the tracking info based on transaction id', function (done) {
      var shipTo = models.newAddress()
        .withCountry(casual.country)
        .withAddress1(casual.address1)
        .withAddress2(casual.address2)
        .withCity(casual.city)
        .withRegion(casual.state_abbr)
        .withFirstName(casual.first_name)
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          assert(res._original);
          assert(res.id);
          return service.getTrackingInfo(res.id)
        })
        .then(function (res) {
          assert(res.trackingUrls);
          assert(res.trackingCodes);
          assert(res.carrier);
          assert(res._original);
          done()
        })
        .catch(function (err) {
          console.log(err);
        });
    });


    it('should reject the promise if the remote service send an error', function (done) {
      service.getTrackingInfo(666)
        .then(function (res) {
          throw new Error('should not get here')
        })
        .catch(function (err) {
          assert.equal(err.message, 'Order not found.');
          done();
        })

    });

  });
});
