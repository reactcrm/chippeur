'use strict';

var Endicia = require('../lib/impls/Endicia');
var client = Endicia({
  requesterId: 'lol',
  accountId: 'lol',
  passphrase: 'lol'
});

client.shipOrder().then(function (result) {
  var label = result.shippingLabel[0];
  require('fs').writeFile('test.png', new Buffer(label, 'base64'))
});
