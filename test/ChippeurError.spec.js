var ChippeurError=require('../lib/errors');
var assert=require('assert');

describe('Chippeur error', function () {


  it('should be instance of Error', function (done) {

    try {
      throw new ChippeurError('blah', {prop: 'value'});
    } catch (e) {
      assert(e instanceof Error);
      assert(e instanceof ChippeurError);
      assert.equal(e.original.prop, 'value');
      assert(e.stack);
      done();
    }

  });
});
