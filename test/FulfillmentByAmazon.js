var credentials = require('./credentials').fba;
var fba = require('../lib/impls/FulfillmentByAmazon.js');
var assert = require('assert');
var models = require('../lib/models');
var casual = require('casual');
var assign = require('object-assign');

xdescribe('fba implementation', function () {


  var service;

  beforeEach(function () {
    service = fba(assign(credentials, {testMode: true}));
  });

  describe('get shipping orders', function () {


    it('should get ', function (done) {

      service.getOrders()
        .then(function (result) {
          assert(result._original, 'original should be defined');
          done();
        })
        .catch(function (err) {
          console.log(err);
        });

    });

  });

  xdescribe('send shipping order', function () {

    it('should send a shipping order', function (done) {

      var shipTo = models.newAddress()
        .withCountry(casual.country)
        .withAddress1(casual.address1)
        .withAddress2(casual.address2)
        .withCity(casual.city)
        .withRegion(casual.state_abbr)
        .withFirstName(casual.first_name)
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          console.log(err);
        });
    });

    it('should reject a promise when the remote service return an error', function (done) {

      var shipTo = models.newAddress()
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('EVIL')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          throw new Error('should not get here');
        })
        .catch(function (err) {
          done();
        });
    });
  });
});

