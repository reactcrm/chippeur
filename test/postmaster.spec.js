var credentials = require('./credentials').postmaster;
var postmaster = require('../lib/impls/Postmaster.js');
var assert = require('assert');
var models = require('../lib/models');
var casual = require('casual');

describe('postmaster implementation', function () {


  var service;

  beforeEach(function () {
    service = postmaster(credentials);
  });


  describe('get shipping orders', function () {

    it('should get ', function (done) {

      service.getOrders()
        .then(function (result) {
          assert(result._original, 'original should be defined');
          done();
        })
        .catch(function (err) {
          console.log(err);
        });

    });

  });

  describe('send shipping order', function () {

    it('should send a shipping order', function (done) {

      var shipTo = models.newAddress()
        .withAddress1('701 Brazos St. Suite 1616')
        .withCity('Austin')
        .withRegion('TX')
        .withFirstName('Joe')
        .withLastName('Smith')
        .withZipCode(78701)
        .withPhoneNumber('512-693-4040');

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withWeightValue(30)
        .withQuantity(1)
        .withLength(4)
        .withHeight(10)
        .withWidth(12);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          assert(res._original, '_original should be defined');
          assert(res.trackingCodes, 'trackingCode should be defined');
          assert(res.id, 'id should be defined');
          done();
        })
        .catch(function (err) {
          console.log(err);
        });
    });

    it('should reject a promise when the remote service return an error', function (done) {

      var shipTo = models.newAddress()
        .withLastName(casual.last_name)
        .withZipCode(casual.zip);

      var pakkage = models.newPackage()
        .withSKU('EVIL')
        .withQuantity(1);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          throw new Error('should not get here');
        })
        .catch(function (err) {
          done();
        });
    });
  });

  describe('void Shipping order', function () {

    it('should void a shipping order', function (done) {

      var shipTo = models.newAddress()
        .withAddress1('701 Brazos St. Suite 1616')
        .withCity('Austin')
        .withRegion('TX')
        .withFirstName('Joe')
        .withLastName('Smith')
        .withZipCode(78701)
        .withPhoneNumber('512-693-4040');

      var pakkage = models.newPackage()
        .withSKU('TEST')
        .withWeightValue(30)
        .withQuantity(1)
        .withLength(4)
        .withHeight(10)
        .withWidth(12);

      service.shipOrder(pakkage, shipTo)
        .then(function (res) {
          assert(res._original, '_original should be defined');
          assert(res.trackingCodes, 'trackingCode should be defined');
          assert(res.id, 'id should be defined');
          return service.voidShippingOrder(res.id)
        })
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          console.log(err);
        })
    });

    it('should reject the promise if the remote service returns an error', function (done) {

      service.voidShippingOrder('6666whatever6666')
        .then(function (result) {
          throw new Error('should not get here');
        })
        .catch(function (err) {
          assert.equal(err.message, 'shipping provider Error');
          assert(err.original)
          done();
        })


    });

  });

  describe('get tracking info', function () {

    it('should get tracking info', function (done) {
      service.getTrackingInfo(5629652273987584)
        .then(function (result) {
          assert(result._original);
          assert(result.trackingCodes);
          assert(result.trackingUrls);
          assert(result.carrier);
          console.log(result);
          done();
        });
    });
  });
});

